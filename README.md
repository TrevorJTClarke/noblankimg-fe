# noblankimg-fe

The website to help the world keep zesty images flowing decentralized all teh way!

## Start Development & Testing Locally

1. Clone the repo and run `npm install`

The repo development is mainly in `/src`.

#### To run local development website:
`npm run dev`
Open the server link printed in cli if it doesnt auto-load in a browser.

#### Helpful Commands
- `npm run build` - Builds compiled client deployable package
