import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import AssetDetail from '@/components/AssetDetail'
import AssetVerification from '@/components/AssetVerification'
import UploadPage from '@/components/UploadPage'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/new',
      name: 'UploadPage',
      component: UploadPage
    },
    {
      path: '/verify',
      name: 'AssetVerification',
      component: AssetVerification
    },
    {
      path: '/:id',
      name: 'AssetDetail',
      component: AssetDetail
    }
  ]
})
