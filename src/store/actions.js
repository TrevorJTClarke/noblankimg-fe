import axios from 'axios'
// import BN from 'bignumber.js'
import Web3 from 'web3'
const abi = require('../../static/contract_abi.json')

// const getAbiDeployedAddress = abi => {
//   const networks = abi.networks
//   return networks[Object.keys(networks)[0]].address
// }
const deployedAddress = '0x926e5614fb7fec020bd712f7a33bf68cc78570ad' // getAbiDeployedAddress(abi)

export default {
  // Connect to either a known web3 provider, or fallback to rinkeby
  connect({ commit, state, dispatch }) {
    let web3Provider = false
    if (typeof window.web3 !== 'undefined') {
      web3Provider = window.web3.currentProvider
      commit('SET_METAMASK', true)
    } else if (!state.retried) {
      commit('SET_RETRY', true)
      setTimeout(() => {
        dispatch('connect')
      }, 1000)
    }
    if (state.retried && !web3Provider) {
      web3Provider = new Web3.providers.HttpProvider(state.constants.rpcUrl)
    }
    if (web3Provider) {
      window.web3 = new Web3(web3Provider)
      commit('SET_CONNECTED', true)
      dispatch('setAccountInterval')
      dispatch('mountContract')
    }
  },

  setAccountInterval({ dispatch }) {
    dispatch('checkAccount')
    setInterval(() => {
      dispatch('checkAccount')
    }, 3000)
  },

  checkAccount({ commit, state }) {
    window.web3.eth.getAccounts((error, accounts) => {
      if (error) console.error(error)
      if (state.account !== accounts[0]) {
        commit('USE_ACCOUNT', accounts[0])
      } else if (!accounts.length) {
        commit('USE_ACCOUNT', null)
      }
    })
  },

  mountContract({ dispatch, commit, state }) {
    if (state.connected) {
      commit('CLEAR_CONTRACT')
      commit('USE_CONTRACT', {
        contract: window.web3.eth.contract(abi.abi),
        address: deployedAddress
      })
      // setTimeout(() => {
      //   dispatch('getStuff')
      // }, 500)
    } else {
      setTimeout(() => {
        dispatch('mountContract')
      }, 500)
    }
  },

  uploadImage({ state }, formData) {
    return axios.post(`${state.constants.apiUrl}/assets`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  },

  async addAsset({ state }, { assetId, digest, functionHash, size }) {
    console.log(assetId, digest, functionHash, size)
    return new Promise(resolve => {
      state.Contract.addAsset(
        assetId,
        digest,
        functionHash,
        size,
        { from: window.web3.eth.accounts[0] },
        tx => {
          resolve(tx)
        }
      )
    })
  },

  getImageMeta({ state }, id) {
    return axios
      .get(`${state.constants.apiUrl}/assets/${id}/meta`)
      .then(r => r.data)
  }
}
