export default {
  account: state => state.account,
  address: state => state.address,
  metamask: state => state.metamask,
  connected: state => state.connected,
  Contract: state => state.Contract,
  ipfsMultiHash: state => state.ipfsMultiHash
}
