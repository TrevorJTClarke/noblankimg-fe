// import abi from '../../build/contracts/REPLACE_THIS.json'

const constants = {
  contractAddress: 'TODO_REPLACE_WITH_DEPLOYED_CONTRACT!',
  rpcUrl: `https://rinkeby.infura.io`,
  apiUrl:
    process.env.NODE_ENV === 'production'
      ? 'https://api.noblankimg.com/v1'
      : 'http://localhost:1337/v1'
}

export default {
  retried: false,
  metamask: false,
  account: null,
  connected: false,
  Contract: null,
  constants
}
